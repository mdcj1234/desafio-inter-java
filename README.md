# Desafio Dígito Único

## Tecnologias utilizadas

* Java 11
* Maven 3.6.3
* H2 database 
* Spring 2.3.4.RELEASE
* Swagger 2.9.2
* Hibernate
* JUnit 4.12

## Como compilar e executar

Primeiramente copie o conteúdo do repositório e compile o sistema com o maven

```
$ git clone https://gitlab.com/mdcj1234/desafio-inter-java.git
$ cd desafio-inter-java
$ mvn package
```

Em seguida para executar o projeto:

```
$ java -jar target/singledigit-1.0.0.jar
```

## Como executar testes unitários

```
$ mvn test
```

## Criando containers

```
$ docker run --rm -p 8080:8080 -d marciodcj94/singledigit
```

## Acessando a documentação do projeto

Após executar o projeto acesse a documentação da API através da url (Supondo que a aplicação esteja da porta 8080):

```
http://localhost:8080/swagger-ui.html
```

## Requisitos da API

* Deverá ser disponibilizado endpoints para o CRUD de usuários. 
    * (POST GET PUT DELETE /users)
* Deverá ser disponibilizado um endpoint para cálculo do dígito, este cálculo pode ser associado de forma opcional a um usuário. 
    * (POST /results)
* Deverá ser criado um endpoint que recupera todos os cálculos para um determinado usuário. 
    * (GET /users/{id}/results)
* Deverá ser criado um endpoint para enviar a chave pública do usuário que será utilizada para a criptografia. Esta API deverá receber uma string que conterá a chave. 
    * (PUT /users)

