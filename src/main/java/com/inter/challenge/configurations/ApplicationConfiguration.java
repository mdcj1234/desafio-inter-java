package com.inter.challenge.configurations;

import com.inter.challenge.providers.cache.Cache;
import com.inter.challenge.providers.cache.LRUCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.inter.challenge.providers.encryption.Encryption;
import com.inter.challenge.providers.encryption.RSAEncryption;
import org.springframework.context.annotation.Scope;

@Configuration
public class ApplicationConfiguration {

	final int cacheCapacity = 10;

	@Bean
	public Encryption encryption() {

		return new RSAEncryption();
	}

	@Bean
	@Scope("singleton")
	public Cache cache() {

		return new LRUCache(cacheCapacity);
	}
}
