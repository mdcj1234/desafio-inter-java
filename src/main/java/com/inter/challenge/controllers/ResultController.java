package com.inter.challenge.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.inter.challenge.dtos.ResultDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.services.ResultService;

import java.util.List;

@RestController
@RequestMapping("/results")
public class ResultController {
	
	@Autowired
	private ResultService resultService;

	@PostMapping
	public ResponseEntity<Result> create(@Valid @RequestBody ResultDTO resultDTO) {
		Result result = resultService.create(resultDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(result);
	}

	@GetMapping
	public List<Result> list() {
		return resultService.list();
	}
}
