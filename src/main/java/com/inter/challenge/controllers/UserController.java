package com.inter.challenge.controllers;

import java.util.List;

import javax.validation.Valid;

import com.inter.challenge.dtos.UserDTO;
import com.inter.challenge.entities.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.inter.challenge.entities.User;
import com.inter.challenge.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;

	@GetMapping
	public List<User> list() {
		return userService.list();
	}

	@GetMapping("/{id}/results")
	public List<Result> findResults(@PathVariable Long id) {
		List<Result> results = userService.findResults(id);
		return results;
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> find(@PathVariable Long id) {
		 User user = userService.find(id);
		 return ResponseEntity.ok(user);
	}
	
	@PostMapping
	public ResponseEntity<User> create(@Valid @RequestBody UserDTO userDTO) {
		User user = userService.create(userDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(user);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<User> update(@PathVariable Long id, @Valid @RequestBody UserDTO userDTO) {
		User user = userService.update(id, userDTO);
		return ResponseEntity.ok(user);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		userService.delete(id);
	}
}
