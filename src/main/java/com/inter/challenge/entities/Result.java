package com.inter.challenge.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity
@Table(name = "results")
public class Result {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Pattern(regexp="^[0-9]*$")
	@Size(max=10000000)
	private String n;

	@NotNull
	private Long k;

	@NotNull
	@Column(name = "single_digit")
	private int singleDigit;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Result() {}

	public Result(Long id, String n, Long k, int singleDigit) {
		this.id = id;
		this.n = n;
		this.k = k;
		this.singleDigit = singleDigit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public Long getK() {
		return k;
	}

	public void setK(Long k) {
		this.k = k;
	}

	public int getSingleDigit() {
		return singleDigit;
	}

	public void setSingleDigit(int singleDigit) {
		this.singleDigit = singleDigit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
