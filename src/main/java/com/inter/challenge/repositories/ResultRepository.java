package com.inter.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inter.challenge.entities.Result;

import java.util.List;

public interface ResultRepository extends JpaRepository<Result, Long>{

    List<Result> findByUser_Id(Long id);

}
