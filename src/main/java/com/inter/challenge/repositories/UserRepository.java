package com.inter.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.inter.challenge.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
