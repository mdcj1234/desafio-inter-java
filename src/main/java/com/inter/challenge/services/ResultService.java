package com.inter.challenge.services;

import java.util.List;
import java.util.Optional;

import com.inter.challenge.exceptions.ApplicationExceptionHandler;
import com.inter.challenge.providers.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.inter.challenge.dtos.ResultDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.entities.User;
import com.inter.challenge.repositories.ResultRepository;
import com.inter.challenge.repositories.UserRepository;
import com.inter.challenge.utils.SingleDigitCalculator;

@Service
public class ResultService {
	
	@Autowired
	ResultRepository resultRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	Cache cache;

	public Result create(ResultDTO resultDTO) {

		Result result = new Result();
		Long k = resultDTO.getK();
		String n = resultDTO.getN();
		Long user_id = resultDTO.getUser_id();

		if(user_id != null) {
			Optional<User> optUser = userRepository.findById(user_id);

			if(optUser.isEmpty()) {
				throw new ApplicationExceptionHandler("User not found", "Invalid ID: " + user_id, HttpStatus.NOT_FOUND);
			}

			result.setUser(optUser.get());
		}

		result.setK(k);
		result.setN(n);

		int cachedSingleDigit = cache.get(n + "-" + k.toString());

		if(cachedSingleDigit != -1){
			result.setSingleDigit(cachedSingleDigit);
		} else {
			int newSingleDigit = SingleDigitCalculator.calculate(n,k);
			result.setSingleDigit(SingleDigitCalculator.calculate(n,k));
			cache.put(n + "-" + k.toString(), newSingleDigit);
		}

		return resultRepository.saveAndFlush(result);
	}

	public List<Result> list() {
		return resultRepository.findAll();
	}
	
}
