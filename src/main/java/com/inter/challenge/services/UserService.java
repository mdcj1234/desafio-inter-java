package com.inter.challenge.services;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Optional;

import com.inter.challenge.dtos.UserDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.exceptions.ApplicationExceptionHandler;
import com.inter.challenge.repositories.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.inter.challenge.entities.User;
import com.inter.challenge.providers.encryption.Encryption;
import com.inter.challenge.repositories.UserRepository;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	ResultRepository resultRepository;
	
	@Autowired
	Encryption encryption;

	public User create(UserDTO userDTO) {

		PublicKey publicKey;
		String encryptedName;
		String encryptedEmail;

		try {
			publicKey = encryption.decodePublicKey(userDTO.getPublicKey());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new ApplicationExceptionHandler("decodePublicKey method error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			encryptedName = encryption.encrypt(userDTO.getName(), publicKey);
			encryptedEmail = encryption.encrypt(userDTO.getEmail(), publicKey);
		} catch (NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeyException e) {
			throw new ApplicationExceptionHandler("encrypt method error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		User user = new User();

		user.setName(encryptedName);
		user.setEmail(encryptedEmail);
		user.setPublicKey(userDTO.getPublicKey());

		return userRepository.saveAndFlush(user);
	}

	public User update(Long id, UserDTO userDTO) {

	    Optional<User> optUser = userRepository.findById(id);

	    if(optUser.isEmpty()) {
			throw new ApplicationExceptionHandler("User not found", "Invalid ID: " + id, HttpStatus.NOT_FOUND);
		}

		PublicKey publicKey;
		String encryptedName;
		String encryptedEmail;

		try {
			publicKey = encryption.decodePublicKey(userDTO.getPublicKey());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new ApplicationExceptionHandler("decodePublicKey method error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			encryptedName = encryption.encrypt(userDTO.getName(), publicKey);
			encryptedEmail = encryption.encrypt(userDTO.getEmail(), publicKey);
		} catch (NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeyException e) {
			throw new ApplicationExceptionHandler("encrypt method error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		User user = optUser.get();

		user.setName(encryptedName);
		user.setEmail(encryptedEmail);
		user.setPublicKey(userDTO.getPublicKey());

		return userRepository.saveAndFlush(user);
	}
	
	public void delete(Long id) {
		Optional<User> optUser = userRepository.findById(id);

		if(optUser.isEmpty()) {
			throw new ApplicationExceptionHandler("User not found", "Invalid ID: " + id, HttpStatus.NOT_FOUND);
		}

		userRepository.delete(optUser.get());

	}
	
	public List<User> list() {
		return userRepository.findAll();
	}
	
	public User find(Long id) {

		Optional<User> optUser = userRepository.findById(id);

		if(optUser.isEmpty()) {
			throw new ApplicationExceptionHandler("User not found", "Invalid ID: " + id, HttpStatus.NOT_FOUND);
		}

		return optUser.get();
	}

	public List<Result> findResults(Long id) {

		Optional<User> optUser = userRepository.findById(id);

		if(optUser.isEmpty()) {
			throw new ApplicationExceptionHandler("User not found", "Invalid ID: " + id, HttpStatus.NOT_FOUND);
		}

		return resultRepository.findByUser_Id(id);
	}
}