package com.inter.challenge.providers.cache;

import java.util.LinkedHashMap;

public class LRUCache implements Cache{

    LinkedHashMap<String, Integer> cache;
    int capacity;

    public LRUCache(int capacity)
    {
        this.cache = new LinkedHashMap<>();
        this.capacity = capacity;
;
    }

    @Override
    public int get(String key) {
        if (!cache.containsKey(key)) return -1;
        int value = cache.get(key);
        put(key, value);
        return value;
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public void put(String key, int value) {
        if (!cache.containsKey(key) && (cache.size() == capacity)) {
            cache.remove(cache.keySet().iterator().next());
        }
        cache.remove(key);
        cache.put(key, value);
    }
}
