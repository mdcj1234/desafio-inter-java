package com.inter.challenge.providers.cache;

public interface Cache {

    int get(String key);
    int size();
    void put(String key, int value);
}
