package com.inter.challenge.providers.encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

public interface Encryption {

	String encrypt(String content, Key pubKey) throws NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, InvalidKeyException;
	String decrypt(String cipherContent, Key privateKey) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
	PublicKey decodePublicKey(String keyStr) throws NoSuchAlgorithmException, InvalidKeySpecException;
}
