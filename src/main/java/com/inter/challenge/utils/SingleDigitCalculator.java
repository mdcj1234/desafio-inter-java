package com.inter.challenge.utils;

public class SingleDigitCalculator {
	
	public static int calculate(String n, Long k) {
		
		String pStr = n;
		Long kAux = k;
		Long p;
		
		do {
			p = 0L;
			
			for(int i = 0; i < pStr.length(); i++) {
				int ascii = pStr.charAt(i);
				p += (ascii - 48);
			}
			
			p *= kAux;
			pStr = p.toString();
			kAux = 1L;
			
		} while (p > 9);

		return Math.toIntExact(p);
	}
}
