package com.inter.challenge.dtos;

import org.springframework.lang.NonNull;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ResultDTO {

	@Size(max = 1000000, min = 1)
	@Pattern(regexp="^[0-9]*$")
	@NonNull
	private String n;

	@Min(1)
	@Max(100000)
	@NonNull
	private Long k;

	@Min(1)
	private Long user_id;

	public ResultDTO() { }

	public ResultDTO(String n, Long k) {
		this.n = n;
		this.k = k;
	}

	public ResultDTO(String n, Long k, Long user_id) {
		this.n = n;
		this.k = k;
		this.user_id = user_id;
	}
	
	public String getN() { return n; }
	public void setN(String n) { this.n = n; }
	public Long getK() {
		return k;
	}
	public void setK(Long k) {
		this.k = k;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

}
