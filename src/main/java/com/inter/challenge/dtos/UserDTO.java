package com.inter.challenge.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDTO {

	private Long id;

	@NotBlank(message = "Name is mandatory")
	@Size(max = 245)
	private String name;

	@NotBlank(message = "Email is mandatory")
	@Size(max = 245)
	@Email
	private String email;

	@NotBlank(message = "PublicKey is mandatory")
	private String publicKey;

	public UserDTO() {}

	public UserDTO(String name, String email, String publicKey) {
		this.email = email;
		this.name = name;
		this.publicKey = publicKey;
	}

	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
}