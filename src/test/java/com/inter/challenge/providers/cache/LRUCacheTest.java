package com.inter.challenge.providers.cache;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LRUCacheTest {

    LRUCache lruCache;
    int capacity = 10;

    @Before
    public void initialize() {
        lruCache= new LRUCache(capacity);
    }

    @Test
    public void shouldBeAbleToStoreValues() {
        assertEquals(lruCache.get("Test"), -1);
        assertEquals(lruCache.size(), 0);
        lruCache.put("Test", 1);
        assertEquals(lruCache.get("Test"), 1);
        assertEquals(lruCache.size(), 1);
    }

    @Test
    public void shouldBeAbleToStoreValuesUpToCapacity() {
        assertEquals(lruCache.size(), 0);
        for(int i = 1; i <= capacity; i++){
            lruCache.put("Test" + i, i);
        }

        for(int i = 1; i <= capacity; i++){
            assertEquals(lruCache.get("Test" + i), i);
        }
        assertEquals(lruCache.size(), capacity);
    }

    @Test
    public void shouldNotConsiderRepeatedValuesInCurrentCapacity() {

        lruCache.put("Test1", 1);
        lruCache.put("Test2", 2);

        lruCache.put("Test1", 1);
        lruCache.put("Test2", 2);

        assertEquals(lruCache.get("Test1"), 1);
        assertEquals(lruCache.get("Test2"), 2);
        assertEquals(lruCache.size(), 2);
    }

    @Test
    public void shouldExcludeLeastRecentUsedValuesWhenFull() {

        for(int i = 1; i <= capacity; i++){
            lruCache.put("Test" + i, i);
        }

        lruCache.put("Test" + (capacity + 1), capacity + 1);
        assertEquals(lruCache.get("Test1"), -1);

        for(int i = 2; i <= capacity + 1; i++){
            assertEquals(lruCache.get("Test" + i), i);
        }

        assertEquals(lruCache.size(), capacity);
    }

    @Test
    public void shouldKeepRecentUsedValues() {

        for(int i = 1; i <= capacity; i++){
            lruCache.put("Test" + i, i);
        }

        for(int i = capacity; i >= 1; i--){
            lruCache.put("Test" + i, i);
        }

        lruCache.put("Test" + (capacity + 1), capacity + 1);
        assertEquals(lruCache.get("Test1"), 1);
        assertEquals(lruCache.get("Test" + capacity), -1);
    }
}
