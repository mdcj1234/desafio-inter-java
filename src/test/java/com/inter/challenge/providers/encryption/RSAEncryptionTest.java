package com.inter.challenge.providers.encryption;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RSAEncryptionTest {

    RSAEncryption rsaEncryption;
    PublicKey publicKey;
    PrivateKey privateKey;

    @Before
    public void initialize() throws NoSuchAlgorithmException {

        rsaEncryption= new RSAEncryption();
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.genKeyPair();
        publicKey = kp.getPublic();
        privateKey = kp.getPrivate();
    }

    @Test
    public void shouldBeAbleToEncryptWithRSA2048Bits() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {

        String message = "Marcio Junior";
        String encryptedMessage = rsaEncryption.encrypt(message, publicKey);
        assertNotEquals(message, encryptedMessage);

        String decryptedMessage = rsaEncryption.decrypt(encryptedMessage, privateKey);
        assertEquals(decryptedMessage, message);
    }
}
