package com.inter.challenge.controllers;

import com.inter.challenge.dtos.UserDTO;
import com.inter.challenge.entities.User;
import com.inter.challenge.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    @Test
    public void shouldBeAbleToCreateUser() {
        UserDTO userDTO = new UserDTO("Test", "valid@email.com.br", "PUBLIC_KEY");

        when(userService.create(any(UserDTO.class))).thenReturn(new User());

        ResponseEntity<User> response = userController.create(userDTO);

        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void shouldBeAbleToUpdateUser() {
        UserDTO userDTO = new UserDTO("Test", "valid@email.com.br", "PUBLIC_KEY");

        when(userService.update(any(Long.class),any(UserDTO.class))).thenReturn(new User());

        ResponseEntity<User> response = userController.update(1L, userDTO);

        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
