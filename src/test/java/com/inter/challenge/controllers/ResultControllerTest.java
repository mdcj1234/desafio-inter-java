package com.inter.challenge.controllers;

import com.inter.challenge.dtos.ResultDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.services.ResultService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResultControllerTest {

    @Mock
    ResultService resultService;

    @InjectMocks
    ResultController resultController;

    @Test
    public void shouldBeAbleToCreateResult() {
        ResultDTO resultDTO = new ResultDTO("9875", 4L);

        when(resultService.create(any(ResultDTO.class))).thenReturn(new Result());

        ResponseEntity<Result> response = resultController.create(resultDTO);

        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

}
