package com.inter.challenge.services;

import com.inter.challenge.dtos.UserDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.entities.User;
import com.inter.challenge.exceptions.ApplicationExceptionHandler;
import com.inter.challenge.providers.encryption.Encryption;
import com.inter.challenge.repositories.ResultRepository;
import com.inter.challenge.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    ResultRepository resultRepository;

    @Mock
    Encryption encryption;

    @InjectMocks
    UserService userService;

    @Before
    public void initialize() throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.genKeyPair();

        when(encryption.decodePublicKey(any(String.class))).thenReturn(kp.getPublic());
        when(encryption.encrypt(any(String.class), any(PublicKey.class))).thenAnswer((Answer) invocation -> {
            Object[] args = invocation.getArguments();
            return "ENCRYPTED " + args[0];
        });

        when(userRepository.saveAndFlush(any(User.class))).thenAnswer((Answer) invocation -> {
            Object[] args = invocation.getArguments();
            return args[0];
        });
    }

    @Test
    public void shouldBeAbleToCreateUserSuccessfully() {
        UserDTO userDTO = new UserDTO("Test", "email@test.com.br", "PUBLIC_KEY");

        User createdUser = userService.create(userDTO);

        assertNotNull(createdUser);
        assertEquals("ENCRYPTED Test", createdUser.getName());
        assertEquals("ENCRYPTED email@test.com.br", createdUser.getEmail());
        assertEquals("PUBLIC_KEY", createdUser.getPublicKey());
    }

    @Test
    public void shouldBeAbleToUpdateUserSuccessfully() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(new User()));

        UserDTO userDTO = new UserDTO("Test", "email@test.com.br", "PUBLIC_KEY");

        User updatedUser = userService.update(1L, userDTO);

        assertNotNull(updatedUser);
        assertEquals("ENCRYPTED Test", updatedUser.getName());
        assertEquals("ENCRYPTED email@test.com.br", updatedUser.getEmail());
        assertEquals("PUBLIC_KEY", updatedUser.getPublicKey());

    }

    @Test
    public void shouldBeAbleToListUsersSuccessfully() {
        when(userRepository.findAll()).thenReturn(List.of(new User()));
        List<User> userList = userService.list();

        assertNotNull(userList);
    }

    @Test
    public void shouldBeAbleToFindUsersSuccessfully() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(new User()));

        User user = userService.find(1L);
        assertNotNull(user);
    }

    @Test
    public void shouldBeAbleToFindUsersResultsSuccessfully() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(new User()));

        when(resultRepository.findByUser_Id(any(Long.class))).thenReturn(List.of(new Result()));

        List<Result> results = userService.findResults(1L);
        assertNotNull(results);
    }

    @Test
    public void shouldBeAbleToDeleteUserSuccessfully() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(new User()));

        userService.delete(1L);
    }

    @Test(expected = ApplicationExceptionHandler.class)
    public void shouldThrowErrorWhenUpdatingWithInvalidId() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        UserDTO userDTO = new UserDTO("Test", "email@test.com.br", "PUBLIC_KEY");

        userService.update(1L, userDTO);
    }

    @Test(expected = ApplicationExceptionHandler.class)
    public void shouldThrowErrorWhenFindingWithInvalidId() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        userService.find(1L);
    }

    @Test(expected = ApplicationExceptionHandler.class)
    public void shouldThrowErrorWhenFindingResultsWithInvalidId() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        userService.findResults(1L);
    }

    @Test(expected = ApplicationExceptionHandler.class)
    public void shouldThrowErrorWhenDeletingWithInvalidId() {
        when(userRepository.findById(any(Long.class))).thenReturn(Optional.empty());

        userService.delete(1L);
    }

}
