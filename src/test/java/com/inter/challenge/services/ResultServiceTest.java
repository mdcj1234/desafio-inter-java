package com.inter.challenge.services;

import com.inter.challenge.dtos.ResultDTO;
import com.inter.challenge.entities.Result;
import com.inter.challenge.entities.User;
import com.inter.challenge.exceptions.ApplicationExceptionHandler;
import com.inter.challenge.providers.cache.Cache;
import com.inter.challenge.repositories.ResultRepository;
import com.inter.challenge.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResultServiceTest {

    @Mock
    ResultRepository resultRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    Cache cache;

    @InjectMocks
    ResultService resultService;

    ResultDTO resultDTO;

    @Before
    public void initialize() {
        when(resultRepository.saveAndFlush(any(Result.class))).thenAnswer((Answer) invocation -> {
            Object[] args = invocation.getArguments();
            return args[0];
        });
    }

    @Test
    public void shouldBeAbleToCreateAResultWithoutUser() {
        resultDTO = new ResultDTO("9875", 4L);

        when(cache.get(any(String.class))).thenReturn(-1);

        Result result = resultService.create(resultDTO);

        assertNotNull(result);
        assertEquals("9875", result.getN());
        assertEquals(0,result.getK().compareTo(4L));
        assertEquals(8, result.getSingleDigit());
    }

    @Test
    public void shouldBeAbleToCreateAResultWithUser() {

        resultDTO = new ResultDTO("9875", 4L, 1L);

        User user = new User();
        user.setId(1L);

        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(user));
        when(cache.get(any(String.class))).thenReturn(-1);

        Result result = resultService.create(resultDTO);

        assertNotNull(result);
        assertEquals("9875", result.getN());
        assertEquals(0,result.getK().compareTo(4L));
        assertEquals(8, result.getSingleDigit());
        assertEquals(user,result.getUser());
    }

    @Test
    public void shouldBeAbleToGetSingleDigitFromCache() {

        resultDTO = new ResultDTO("9875", 4L);

        when(cache.get(any(String.class))).thenReturn(9); // wrong in purpose

        Result result = resultService.create(resultDTO);

        assertNotNull(result);
        assertEquals("9875", result.getN());
        assertEquals(0,result.getK().compareTo(4L));
        assertEquals(9, result.getSingleDigit());
    }

    @Test
    public void shouldBeAbleToListUsersSuccessfully() {
        when(resultRepository.findAll()).thenReturn(List.of(new Result()));
        List<Result> resultList = resultService.list();

        assertNotNull(resultList);
    }

    @Test(expected = ApplicationExceptionHandler.class)
    public void shouldThrowErrorCreatingResultWithInvalidUserId() {

        resultDTO = new ResultDTO("9875", 4L, 1L);

        resultService.create(resultDTO);
    }
}
