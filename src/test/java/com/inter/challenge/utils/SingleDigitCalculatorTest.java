package com.inter.challenge.utils;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class SingleDigitCalculatorTest {

    @Test
    public void calculationsTest() {
        assertEquals(SingleDigitCalculator.calculate("9875", (long) 1), 2);
        assertEquals(SingleDigitCalculator.calculate("9875", (long) 4), 8);
        assertEquals(SingleDigitCalculator.calculate("1000000", (long) 9), 9);
        assertEquals(SingleDigitCalculator.calculate("1111", (long) 5), 2);
        assertEquals(SingleDigitCalculator.calculate("1111", (long) 5), 2);
        assertEquals(SingleDigitCalculator.calculate("1", (long) 100000), 1);
        assertEquals(SingleDigitCalculator.calculate("2", (long) 100000), 2);
    }
}
